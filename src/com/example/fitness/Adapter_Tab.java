package com.example.fitness;



import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class Adapter_Tab extends FragmentPagerAdapter {

	public Adapter_Tab(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {
		switch (index) {
        case 0:
            // Top Rated fragment activity
            return new Panduan1();
        case 1:
            // Games fragment activity
        	return new Panduan2();
        case 2:
            // Games fragment activity
        	return new Panduan3();
        case 3:
            // Games fragment activity
        	return new Panduan4();
        }
 
        return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 4;
	}
	
	@Override
    public CharSequence getPageTitle(int position) {
        //return "Page #" + ( position + 1 );
    	switch (position) {
        case 0:
            // Top Rated fragment activity
        	return "Tentang";
        case 1:
            // Games fragment activity
        	return "Penggunaan";
        case 2:
            // Top Rated fragment activity
        	return "Intruksi Tes";
        case 3:
            // Games fragment activity
        	return "Peringatan";
        }
 
        return null;
    }

}
