package com.example.fitness;

import java.util.ArrayList;
import java.util.List;
import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class Pelari extends Activity implements OnClickListener, OnRefreshListener, OnItemClickListener, android.widget.AdapterView.OnItemLongClickListener{

	Button back,tambah;
	ListView list;
	int simpan_gender,simpan_usia;
	String simpan_nama;
	SwipeRefreshLayout swipeRefreshLayout;
	List<Data_Pelari> dataku;
	long id_dipilih;
	//Data_Pelari data;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_pelari);
		
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);
		
		back = (Button) findViewById(R.id.back);
		back.setOnClickListener(this);
		tambah = (Button) findViewById(R.id.tambah);
		tambah.setOnClickListener(this);
		list = (ListView) findViewById(R.id.list);
		list.setOnItemClickListener(this);
		list.setOnItemLongClickListener(this);
		
		dataku = Data_Pelari.listAll(Data_Pelari.class);
		Adapter_Daftar_Pelari adapter = new Adapter_Daftar_Pelari(Pelari.this, dataku);
		list.setAdapter(adapter);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back:
			onBackPressed();
			break;
		case R.id.tambah:
			tambah_pelari();
			break;

		}
	}

	public void tambah_pelari()
	{
		final Dialog dialog = new Dialog(this);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    dialog.setContentView(R.layout.model_tambah_pelari);
	    
	    final EditText nama = (EditText) dialog.findViewById(R.id.edittext_nama);
	    final EditText umur = (EditText) dialog.findViewById(R.id.edittext_umur);
	    
	    
	    final RadioButton rb1 = (RadioButton) dialog.findViewById(R.id.rb_lakilaki);
	    RadioButton rb2 = (RadioButton) dialog.findViewById(R.id.rb_perempuan);
	    rb1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(rb1.isChecked()==true)
			    {
			    	simpan_gender = 1;
			    	//Toast.makeText(getApplicationContext(), "Laki-laki", Toast.LENGTH_SHORT).show();
			    }
			    else
			    {
			    	//Toast.makeText(getApplicationContext(), "Perempuan", Toast.LENGTH_SHORT).show();
			    	simpan_gender = 0;
			    }
			}
		});
	    
	    	    
	    Button simpan = (Button) dialog.findViewById(R.id.simpan);
	    simpan.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				simpan_nama = nama.getText().toString();
			    simpan_usia = Integer.parseInt(umur.getText().toString());
			    if(simpan_usia>9 && simpan_usia<31)
			    {
			    	Data_Pelari data = new Data_Pelari(simpan_nama,simpan_usia,simpan_gender,1,0);
					data.save();
					long x = data.getId();
					Data_Pelari data2 = Data_Pelari.findById(Data_Pelari.class, x);
					data2.id = x;
					data2.save();
					dialog.dismiss();
					onRefresh();
			    }
			    else
			    {
			    	Toast.makeText(getApplicationContext(), "Masukan usia antara 10 - 30 tahun", Toast.LENGTH_SHORT).show();
			    }
				
				
			}
		});
	    
		dialog.show();
	}
	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		swipeRefreshLayout.setRefreshing(true);
		List<Data_Pelari> dataku= Data_Pelari.listAll(Data_Pelari.class);
		Adapter_Daftar_Pelari adapter = new Adapter_Daftar_Pelari(Pelari.this, dataku);
		list.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		swipeRefreshLayout.setRefreshing(false);
		
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int posisi, long arg3) {
		// TODO Auto-generated method stub
		long x = dataku.get(posisi).id;
		Data_Pelari data = Data_Pelari.findById(Data_Pelari.class, x);
		if(data.aktif==0)
		{
			data.aktif=1;
		}
		else
		{
			data.aktif=0;
		}
		data.save();
		onRefresh();
	}
	
	public void dialog()
	{
		final Dialog dialog = new Dialog(this);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    dialog.setContentView(R.layout.model_dialog_pilihan);
	    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	    Button edit = (Button) dialog.findViewById(R.id.edit);
	    edit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				edit_pelari();
				
			}
		});
	    Button hapus = (Button) dialog.findViewById(R.id.hapus);
	    hapus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_hapus();
				dialog.dismiss();
				
			}
		});
	    
		dialog.show();
	}
	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int posisi,
			long arg3) {
		// TODO Auto-generated method stub
		id_dipilih = dataku.get(posisi).id;
		dialog();
		return true;
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		Intent intent = new Intent(Pelari.this,Test.class);
		startActivity(intent);
	}
	
	public void dialog_hapus()
	{
		final Dialog dialog = new Dialog(this);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    dialog.setContentView(R.layout.model_dialog_hapus);
	    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	    Button ya = (Button) dialog.findViewById(R.id.ya);
	    ya.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				Data_Pelari.deleteAll(Data_Pelari.class,"id = ?",""+id_dipilih);
				onRefresh();
				
			}
		});
	    Button tidak = (Button) dialog.findViewById(R.id.tidak);
	    tidak.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				
			}
		});
	    
		dialog.show();
	}
	
	
	public void edit_pelari()
	{
		final Data_Pelari dataz = Data_Pelari.findById(Data_Pelari.class, id_dipilih);
		
		final Dialog dialog = new Dialog(this);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    dialog.setContentView(R.layout.model_tambah_pelari);
	    
	    final EditText namaz = (EditText) dialog.findViewById(R.id.edittext_nama);
	    namaz.setText(""+dataz.nama);
	    final EditText umurz = (EditText) dialog.findViewById(R.id.edittext_umur);
	    umurz.setText(""+dataz.usia);
	    
	    final RadioButton rb1 = (RadioButton) dialog.findViewById(R.id.rb_lakilaki);
	    RadioButton rb2 = (RadioButton) dialog.findViewById(R.id.rb_perempuan);
	    if(dataz.gender==0)
	    {
	    	rb2.setChecked(true);
	    }
	    else
	    {
	    	rb1.setChecked(true);
	    }
	    rb1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(rb1.isChecked()==true)
			    {
			    	simpan_gender = 1;
			    	//Toast.makeText(getApplicationContext(), "Laki-laki", Toast.LENGTH_SHORT).show();
			    }
			    else
			    {
			    	//Toast.makeText(getApplicationContext(), "Perempuan", Toast.LENGTH_SHORT).show();
			    	simpan_gender = 0;
			    }
			}
		});
	    
	    	    
	    Button simpan = (Button) dialog.findViewById(R.id.simpan);
	    simpan.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				simpan_nama = namaz.getText().toString();
			    simpan_usia = Integer.parseInt(umurz.getText().toString());
			    if(simpan_usia>9 && simpan_usia<31)
			    {
			    	dataz.nama = simpan_nama;
			    	dataz.usia = simpan_usia;
			    	dataz.gender = simpan_gender;
			    	dataz.save();
			    	dialog.dismiss();
			    	onRefresh();
			    }
			    else
			    {
			    	Toast.makeText(getApplicationContext(), "Masukan usia antara 10 - 30 tahun", Toast.LENGTH_SHORT).show();
			    }
				
				
			}
		});
	    
		dialog.show();
	}

}
