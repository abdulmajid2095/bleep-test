package com.example.fitness;

import java.text.SimpleDateFormat;																																																																																																																																																																																																																																																																																																																			
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Test extends Activity implements OnClickListener, OnRefreshListener, OnItemClickListener{

	Button start,stop,edit,back;
	TextView teks_jarak,teks_level;
	MediaPlayer beep,awal;
	
	Chronometer chronometer;
	long stopTime = 0;
	long waktu;
	
	boolean test_status=false;
	
	private CountDownTimer countDownTimer;
	long startTime = 9000;
	private final long interval = 1 * 100;
	
	int jarak=0;
	int level = 1;
	int shuttle = 1;
	int jumlah_shuttle = 7;
	long detik,menit;
	int jumlah_data;
	
	long id_dipilih;
	String tanggal;
	Boolean kondisi=false;
	double vo2;
	int usia_dipilih;
	String rating_dipilih;
	int selesai = 0;
	ListView list;
	SwipeRefreshLayout swipeRefreshLayout;
	List<Data_Pelari> dataku;
	
	String[] x_nama;
	String[] x_usia;
	String[] x_tanggal;
	String[] x_level;
	String[] x_shuttle;
	String[] x_vo2;
	String[] x_rating;
	String[] x_id;
	int x_disimpan = 0;
	int jumlah_fix;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_test);
		
		
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);
		dataku = Data_Pelari.find(Data_Pelari.class, "aktif = ?", "1");
		jumlah_data = (int) Data_Pelari.count(Data_Pelari.class, "aktif = 1", null);
		//Toast.makeText(getApplicationContext(), ""+jumlah_data, Toast.LENGTH_SHORT).show();
		Adapter_test adapter = new Adapter_test(Test.this, dataku);
		list = (ListView) findViewById(R.id.list);
		list.setOnItemClickListener(this);
		list.setAdapter(adapter);
		
		start = (Button) findViewById(R.id.b_start);
		start.setOnClickListener(this);
		stop = (Button) findViewById(R.id.b_stop);
		stop.setOnClickListener(this);
		edit = (Button) findViewById(R.id.edit);
		edit.setOnClickListener(this);
		back = (Button) findViewById(R.id.back);
		back.setOnClickListener(this);
		
		teks_jarak = (TextView) findViewById(R.id.teks_jarak);
		teks_level = (TextView) findViewById(R.id.teks_level);
		
		chronometer = (Chronometer)findViewById(R.id.chronometer);
		countDownTimer = new MyCountDownTimer(startTime, interval);
		
		get_tanggal();
		
		x_nama = new String[jumlah_data];
		x_usia = new String[jumlah_data];
		x_tanggal = new String[jumlah_data];
		x_level = new String[jumlah_data];
		x_shuttle = new String[jumlah_data];
		x_vo2 = new String[jumlah_data];
		x_rating = new String[jumlah_data];
		x_id = new String[jumlah_data];
		jumlah_fix = jumlah_data;
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.b_start:
			if(jumlah_data==0)
			{
				Toast.makeText(getApplicationContext(), "Pilih minimal 1 pelari", Toast.LENGTH_SHORT).show();
			}
			else
			{
				start_fitness();
			}
			
			break;
		case R.id.b_stop:
			test_status = false;
			simpan_semua();
			stop_fitness();
			dialog_hasil();
			break;
			
		case R.id.edit:
			Intent intent = new Intent(Test.this,Pelari.class);
			startActivity(intent);
			break;
			
		case R.id.back:
			onBackPressed();
			break;
		}
	}
	
	public void start_fitness()
	{
		kondisi = true;
		
		awal = MediaPlayer.create(getApplicationContext(), R.raw.awal);
		awal.setVolume(1, 1);
		awal.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub
				MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level1);
				slevel.setVolume(1, 1);
				slevel.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						awal.release();
						countDownTimer.start();
			            chronometer.setBase(SystemClock.elapsedRealtime() + stopTime);
			            chronometer.start();
						test_status = true;
			            teks_jarak.setText(jarak+" m");
						teks_level.setText(level+"."+shuttle);
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				slevel.start();
			}
		});
		awal.start();
        start.setVisibility(View.GONE);
        stop.setVisibility(View.VISIBLE);
	}
	public void stop_fitness()
	{
		
		//untuk pause, set stoptime;
		//stopTime = chronometer.getBase() - SystemClock.elapsedRealtime();
		teks_jarak.setText("0 m");
		teks_level.setText("0.0");
		kondisi = false;
				
		waktu = SystemClock.elapsedRealtime() - chronometer.getBase();
		chronometer.setBase(SystemClock.elapsedRealtime());
        stopTime = 0;
        chronometer.stop();
        start.setVisibility(View.VISIBLE);
        stop.setVisibility(View.GONE);
        countDownTimer.cancel();
        
        long sec = waktu/1000;
        menit = sec/60;
        long sec2 = menit*60;
        detik = sec-sec2;
        Toast.makeText(getApplicationContext(), "Test Dihentikan", Toast.LENGTH_LONG).show();
	}
	
	
	class MyCountDownTimer extends CountDownTimer {
		public MyCountDownTimer(long startTime, long interval) {
			super(startTime, interval);
		}

		@Override
		public void onFinish() {
			cek_level();
		}

		@Override
		public void onTick(long millisUntilFinished) {
			
	}
	}
	
	public void cek_level()
	{
		if(level==1)
		{
			jarak=jarak+20;
			startTime = 9000;
			shuttle = shuttle+1;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level2);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				
				startTime = 8000;
				level = 2;
				shuttle=1;
				jumlah_shuttle=8;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==2)
		{
			startTime = 8000;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level3);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 7580;
				level = 3;
				shuttle=1;
				jumlah_shuttle=8;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		
		else if(level==3)
		{
			startTime = 7580;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level4);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 7200;
				level = 4;
				shuttle=1;
				jumlah_shuttle=9;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		
		else if(level==4)
		{
			startTime = 7200;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level5);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 6860;
				level = 5;
				shuttle=1;
				jumlah_shuttle=9;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==5)
		{
			startTime = 6860;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level6);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 6550;
				level = 6;
				shuttle=1;
				jumlah_shuttle=10;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==6)
		{
			startTime = 6550;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level7);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 6260;
				level = 7;
				shuttle=1;
				jumlah_shuttle=10;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==7)
		{
			startTime = 6260;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level8);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 6000;
				level = 8;
				shuttle=1;
				jumlah_shuttle=11;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==8)
		{
			startTime = 6000;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level9);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 5760;
				level = 9;
				shuttle=1;
				jumlah_shuttle=11;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==9)
		{
			startTime = 5760;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level10);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 5540;
				level = 10;
				shuttle=1;
				jumlah_shuttle=11;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==10)
		{
			startTime = 5540;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level11);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 5330;
				level = 11;
				shuttle=1;
				jumlah_shuttle=12;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==11)
		{
			startTime = 5330;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level12);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 5140;
				level = 12;
				shuttle=1;
				jumlah_shuttle=12;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==12)
		{
			startTime = 5140;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level13);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 4970;
				level = 13;
				shuttle=1;
				jumlah_shuttle=13;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==13)
		{
			startTime = 4970;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level14);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 4800;
				level = 14;
				shuttle=1;
				jumlah_shuttle=13;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==14)
		{
			startTime = 4800;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level15);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 4650;
				level = 15;
				shuttle=1;
				jumlah_shuttle=13;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==15)
		{
			startTime = 4650;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level16);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 4500;
				level = 16;
				shuttle=1;
				jumlah_shuttle=14;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==16)
		{
			startTime = 4500;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level17);
						slevel.setVolume(1, 1);
					  	slevel.start();
					}
				});
				beep.start();
				startTime = 4360;
				level = 17;
				shuttle=1;
				jumlah_shuttle=14;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==17)
		{
			startTime = 4360;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level18);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 4240;
				level = 18;
				shuttle=1;
				jumlah_shuttle=15;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==18)
		{
			startTime = 4240;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level19);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 4110;
				level = 19;
				shuttle=1;
				jumlah_shuttle=15;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==19)
		{
			startTime = 4110;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level20);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 4000;
				level = 20;
				shuttle=1;
				jumlah_shuttle=16;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==20)
		{
			startTime = 4000;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						MediaPlayer slevel = MediaPlayer.create(getApplicationContext(), R.raw.level21);
						slevel.setVolume(1, 1);
						slevel.start();
					}
				});
				beep.start();
				startTime = 3890;
				level = 21;
				shuttle=1;
				jumlah_shuttle=16;
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		else if(level==21)
		{
			startTime = 3890;
			shuttle = shuttle+1;
			jarak = jarak+20;
			if(shuttle>jumlah_shuttle)
			{
				stop_fitness();
				selesai = 1;
				simpan_semua();
			}
			else if(shuttle==jumlah_shuttle)
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
						beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
						beep.setVolume(1, 1);
						beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								beep.release();
							}
						});
						beep.start();
					}
				});
				beep.start();
			}
			else
			{
				beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
				beep.setVolume(1, 1);
				beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						beep.release();
					}
				});
				beep.start();
			}
		}
		if(selesai == 0)
		{
			teks_jarak.setText(jarak+" m");
			teks_level.setText(level+"."+shuttle);
			countDownTimer = new MyCountDownTimer(startTime, interval);
			countDownTimer.start();
		}
		
		
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(kondisi==true)
		{
			Toast.makeText(getApplicationContext(), "Test masih berjalan. Stop terlebih dahulu.", Toast.LENGTH_LONG).show();
		}
		else
		{
			Intent intent = new Intent(Test.this,Utama.class);
			startActivity(intent);
		}
		
	}
	
	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		swipeRefreshLayout.setRefreshing(true);
		dataku = Data_Pelari.find(Data_Pelari.class, "aktif = ?", "1");
		Adapter_test adapter = new Adapter_test(Test.this, dataku);
		list.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		swipeRefreshLayout.setRefreshing(false);
		
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int posisi, long arg3) {
		// TODO Auto-generated method stub
		if(test_status==true)
		{
			id_dipilih = dataku.get(posisi).id;
			Data_Pelari data = Data_Pelari.findById(Data_Pelari.class, id_dipilih);
			data.aktif=0;
			data.save();
			vo2 = 3.46*(level+shuttle/(level*0.4325+7.0048))+12.2;
			usia_dipilih = data.usia;
			if(data.gender==1)
			{
				cek_pria();
			}
			else
			{
				cek_wanita();
			}
			/*Data_Hasil data2 = new Data_Hasil(""+data.nama,""+data.usia,""+tanggal,level+"."+shuttle,""+vo2,""+rating_dipilih,0);
			data2.save();
			long x = data2.getId();
			Data_Hasil data3 = Data_Hasil.findById(Data_Hasil.class, x);
			data3.id = x;
			data3.save();*/
			
			x_nama[x_disimpan] = data.nama;
			x_usia[x_disimpan] = ""+data.usia;
			x_tanggal[x_disimpan] = tanggal;
			x_level[x_disimpan] = ""+level;
			x_shuttle[x_disimpan] = ""+shuttle;
			x_vo2[x_disimpan] = ""+vo2;
			x_rating[x_disimpan] = ""+rating_dipilih;
			x_disimpan = x_disimpan+1;
			
			jumlah_data = jumlah_data-1;
			if(jumlah_data==0)
			{
				stop_fitness();
				dialog_hasil();
			}
			onRefresh();
		}
	}
	
	public void get_tanggal()
	{
		Date c = Calendar.getInstance().getTime();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		tanggal = df.format(c);
		
	}
	
	public void cek_wanita()
	{
		int myString1 = Integer.parseInt(level+"0");
		int myCek = myString1+shuttle;
		
		if(usia_dipilih == 10)
		{
			if(myCek<32)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>31 && myCek<39)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>40 && myCek<48)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>47 && myCek<57)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>56 && myCek<69)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 11)
		{
			if(myCek<29)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>30 && myCek<39)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>40 && myCek<49)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>48 && myCek<59)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>58 && myCek<72)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 12)
		{
			if(myCek<29)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>30 && myCek<42)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>41 && myCek<50)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>50 && myCek<60)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>60 && myCek<74)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 13)
		{
			if(myCek<32)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>31 && myCek<42)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>41 && myCek<52)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>51 && myCek<62)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>61 && myCek<76)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 14)
		{
			if(myCek<32)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>31 && myCek<42)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>41 && myCek<52)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>51 && myCek<63)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>62 && myCek<677)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 15)
		{
			if(myCek<32)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>31 && myCek<43)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>42 && myCek<53)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>52 && myCek<64)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>63 && myCek<78)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 16)
		{
			if(myCek<32)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>31 && myCek<43)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>42 && myCek<53)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>52 && myCek<64)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>63 && myCek<79)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 17)
		{
			if(myCek<32)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>31 && myCek<43)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>42 && myCek<54)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>53 && myCek<65)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>64 && myCek<80)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}	
		}
		
		//Usia 18 - 25
		if(usia_dipilih >17 && usia_dipilih <26)
		{
			if(myCek<58)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>54 && myCek<73)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>72 && myCek<87)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>86 && myCek<102)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>101 && myCek<128)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		//Usia 26 - 30
		if(usia_dipilih >25 && usia_dipilih <31)
		{
			if(myCek<53)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>52 && myCek<66)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>65 && myCek<78)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>77 && myCek<95)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>94 && myCek<116)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
	}
	
	public void cek_pria()
	{
		int myString1 = Integer.parseInt(level+"0");
		int myCek = myString1+shuttle;
		
		if(usia_dipilih == 10)
		{
			if(myCek<36)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>35 && myCek<46)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>45 && myCek<56)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>55 && myCek<67)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>66 && myCek<82)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 11)
		{
			if(myCek<37)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>36 && myCek<46)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>45 && myCek<59)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>58 && myCek<71)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>70 && myCek<87)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 12)
		{
			if(myCek<39)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>37 && myCek<48)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>47 && myCek<65)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>64 && myCek<77)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>76 && myCek<94)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}	
		}
		
		if(usia_dipilih == 13)
		{
			if(myCek<43)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>42 && myCek<57)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>56 && myCek<70)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>69 && myCek<84)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>83 && myCek<102)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}	
		}
		
		if(usia_dipilih == 14)
		{
			if(myCek<46)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>45 && myCek<62)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>61 && myCek<75)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>74 && myCek<90)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>89 && myCek<110)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}	
		}
		
		if(usia_dipilih == 15)
		{
			if(myCek<48)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>47 && myCek<64)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>63 && myCek<78)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>77 && myCek<93)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>92 && myCek<114)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}	
		}
		
		if(usia_dipilih == 16)
		{
			if(myCek<50)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>50 && myCek<67)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>66 && myCek<81)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>80 && myCek<97)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>96 && myCek<119)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}		
		}
		
		if(usia_dipilih == 17)
		{
			if(myCek<53)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>52 && myCek<69)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>68 && myCek<84)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>83 && myCek<100)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>99 && myCek<122)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}	
		}
		
		//Usia 18 - 25
		if(usia_dipilih >17 && usia_dipilih <26)
		{
			if(myCek<72)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>70 && myCek<86)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>85 && myCek<102)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>101 && myCek<116)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>115 && myCek<140)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}	
		}
		
		//Usia 26 - 30
		if(usia_dipilih >25 && usia_dipilih <31)
		{
			if(myCek<66)
			{
				rating_dipilih = "Lemah";
			}
			else if(myCek>65 && myCek<80)
			{
				rating_dipilih = "Cukup";
			}
			else if(myCek>79 && myCek<90)
			{
				rating_dipilih = "Rata-rata";
			}
			else if(myCek>89 && myCek<107)
			{
				rating_dipilih = "Baik";
			}
			else if(myCek>106 && myCek<130)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}	
		}
		
	}
	
	/*public void cek_wanita()
	{
		if(usia_dipilih == 10)
		{
			if(vo2<22.6372)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=22.6372 && vo2<25.9140)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=25.9140 && vo2<28.8128)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=28.8128 && vo2<31.7646)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=31.7646 && vo2<35.8434)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 11)
		{
			if(vo2<22.6372)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=22.6372 && vo2<25.9140)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=25.9140 && vo2<29.2089)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=29.2089 && vo2<32.5194)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=32.5194 && vo2<36.7649)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 12)
		{
			if(vo2<22.6372)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=22.6372 && vo2<26.4361)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=26.4361 && vo2<29.6051)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=29.6051 && vo2<32.8969)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=32.8969 && vo2<37.4547)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 13)
		{
			if(vo2<22.9968)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=22.9968 && vo2<26.4361)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=26.4361 && vo2<29.8774)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=29.8774 && vo2<33.3204)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=33.3204 && vo2<38.1444)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 14)
		{
			if(vo2<22.9968)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=22.9968 && vo2<26.4361)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=26.4361 && vo2<29.8774)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=29.8774 && vo2<33.6808)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=33.6808 && vo2<38.4893)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 15)
		{
			if(vo2<22.9968)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=22.9968 && vo2<26.8322)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=26.8322 && vo2<30.2549)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=30.2549 && vo2<34.0413)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=34.0413 && vo2<38.8342)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 16)
		{
			if(vo2<22.9968)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=22.9968 && vo2<26.8322)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=26.8322 && vo2<30.2549)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=30.2549 && vo2<34.0413)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=34.0413 && vo2<39.1791)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 17)
		{
			if(vo2<22.9968)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=22.9968 && vo2<26.8322)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=26.8322 && vo2<30.6323)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=30.6323 && vo2<34.4017)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=34.4017 && vo2<39.5240)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		//Usia 18 - 25
		if(usia_dipilih >17 && usia_dipilih <26)
		{
			if(vo2<32.1420)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=32.1420 && vo2<37.1098)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=37.1098 && vo2<41.8638)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=41.8638 && vo2<47.1054)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=47.1054 && vo2<55.7061)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		//Usia 26 - 30
		if(usia_dipilih >25 && usia_dipilih <31)
		{
			if(vo2<30.2549)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=30.2549 && vo2<34.7621)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=34.7621 && vo2<38.8342)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=38.8342 && vo2<44.6100)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=44.6100 && vo2<51.7308)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
	}*/
	
	/*public void cek_pria()
	{
		if(usia_dipilih == 10)
		{
			if(vo2<24.6638)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=24.6638 && vo2<28.0206)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=28.0206 && vo2<31.3871)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=31.3871 && vo2<35.1225)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=35.1225 && vo2<40.2106)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 11)
		{
			if(vo2<25.0805)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=25.0805 && vo2<28.8128)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=28.8128 && vo2<32.5194)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=32.5194 && vo2<36.5642)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=36.5642 && vo2<41.8638)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 12)
		{
			if(vo2<25.9140)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=25.9140 && vo2<29.8774)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=29.8774 && vo2<34.4017)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=34.4017 && vo2<38.4893)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=38.4893 && vo2<44.2925)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 13)
		{
			if(vo2<26.8322)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=26.8322 && vo2<31.7646)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=31.7646 && vo2<36.2038)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=36.2038 && vo2<40.8719)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=40.8719 && vo2<47.1054)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 14)
		{
			if(vo2<28.0206)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=28.0206 && vo2<33.3204)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=33.3204 && vo2<37.7995)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=37.7995 && vo2<42.8557)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=42.8557 && vo2<49.5485)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 15)
		{
			if(vo2<28.8128)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=28.8128 && vo2<34.0413)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=34.0413 && vo2<38.8342)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=38.8342 && vo2<43.9750)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=43.9750 && vo2<51.1425)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 16)
		{
			if(vo2<29.6051)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=29.6051 && vo2<35.1225)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=35.1225 && vo2<39.8689)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=39.8689 && vo2<45.2451)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=45.2451 && vo2<52.6133)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		if(usia_dipilih == 17)
		{
			if(vo2<30.2549)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=30.2549 && vo2<35.8434)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=35.8434 && vo2<40.8719)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=40.8719 && vo2<46.1976)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=46.1976 && vo2<54.0037)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		//Usia 18 - 25
		if(usia_dipilih >17 && usia_dipilih <26)
		{
			if(vo2<36.7649)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=36.7649 && vo2<41.5332)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=41.5332 && vo2<47.1054)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=47.1054 && vo2<51.7308)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=51.7308 && vo2<59.9201)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
		//Usia 26 - 30
		if(usia_dipilih >25 && usia_dipilih <31)
		{
			if(vo2<34.7621)
			{
				rating_dipilih = "Lemah";
			}
			else if(vo2>=34.7621 && vo2<39.5240)
			{
				rating_dipilih = "Cukup";
			}
			else if(vo2>=39.5240 && vo2<42.8557)
			{
				rating_dipilih = "Rata-Rata";
			}
			else if(vo2>=42.8557 && vo2<48.6323)
			{
				rating_dipilih = "Baik";
			}
			else if(vo2>=48.6323 && vo2<56.2738)
			{
				rating_dipilih = "Baik Sekali";
			}
			else
			{
				rating_dipilih = "Unggul";
			}
		}
		
	}*/
	
	public void simpan_semua()
	{
		for(int z = 0; z < jumlah_data; z++)
		{
			long id = dataku.get(z).id;
			Data_Pelari data = Data_Pelari.findById(Data_Pelari.class, id);
			data.aktif=0;
			data.save();
			vo2 = 3.46*(level+shuttle/(level*0.4325+7.0048))+12.2;
			usia_dipilih = data.usia;
			if(data.gender==1)
			{
				cek_pria();
			}
			else
			{
				cek_wanita();
			}
			
			/*Data_Hasil data2 = new Data_Hasil(""+data.nama,""+data.usia,""+tanggal,level+"."+shuttle,""+vo2,""+rating_dipilih,0);
			data2.save();
			long x = data2.getId();
			Data_Hasil data3 = Data_Hasil.findById(Data_Hasil.class, x);
			data3.id = x;
			data3.save();*/
			
			x_nama[x_disimpan] = data.nama;
			x_usia[x_disimpan] = ""+data.usia;
			x_tanggal[x_disimpan] = tanggal;
			x_level[x_disimpan] = ""+level;
			x_shuttle[x_disimpan] = ""+shuttle;
			x_vo2[x_disimpan] = ""+vo2;
			x_rating[x_disimpan] = ""+rating_dipilih;
			x_disimpan = x_disimpan+1;
			
		}
		jumlah_data = 0;
		onRefresh();
	}
	
	
	public void dialog_hasil()
	{
		final Dialog dialog = new Dialog(this);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    dialog.setContentView(R.layout.dialog_hasil);
	    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	    
	    ListView mylist = (ListView) dialog.findViewById(R.id.list);
		Adapter_Dialog adapter = new Adapter_Dialog(this, x_nama, x_tanggal, x_level, x_shuttle, x_vo2, x_rating, x_usia);
		mylist.setAdapter(adapter);
	    
	    Button ya = (Button) dialog.findViewById(R.id.simpan);
	    ya.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				simpan();
				dialog_berhasil_simpan();
			}
		});
	    Button tidak = (Button) dialog.findViewById(R.id.batal);
	    tidak.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				
			}
		});
	    
		dialog.show();
	}
	
	public void simpan()
	{
		for(int i = 0; i<jumlah_fix; i++)
		{
			Data_Hasil data2 = new Data_Hasil(""+x_nama[i],""+x_usia[i],""+x_tanggal[i],x_level[i]+"."+x_shuttle[i],""+x_vo2[i],""+x_rating[i],0);
			data2.save();
			long x = data2.getId();
			Data_Hasil data3 = Data_Hasil.findById(Data_Hasil.class, x);
			data3.id = x;
			data3.save();
		}
		
	}
	
	public void dialog_berhasil_simpan()
	{
		final Dialog dialog = new Dialog(this);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    dialog.setContentView(R.layout.model_berhasil_simpan);
	    	    
	    Button ya = (Button) dialog.findViewById(R.id.ya);
	    ya.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				Intent intent = new Intent(Test.this,Hasil.class);
				startActivity(intent);
			}
		});
	    
		dialog.show();
	}

}
