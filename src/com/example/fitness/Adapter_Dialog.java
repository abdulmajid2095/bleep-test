package com.example.fitness;

import java.text.DecimalFormat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Adapter_Dialog extends ArrayAdapter<String> implements OnClickListener {

	
	String[] nama={};
	String[] tanggal={};
	String[] level={};
	String[] shuttle={};
	String[] vo2={};
	String[] rating={};
	String[] usia={};
	Context c;
	LayoutInflater inflater;
	
	
	public Adapter_Dialog(Context context, String[]nama, String[]tanggal, String[]level, String[]shuttle, String[]vo2, String[]rating, String[]usia) {
		super(context, R.layout.model_hasil,nama);
		
		this.c = context;
		this.nama = nama;
		this.tanggal = tanggal;
		this.level = level;
		this.shuttle = shuttle;
		this.vo2 = vo2;
		this.rating = rating;
		this.usia = usia;
	}
	
	public class ViewHolder
	{
		TextView nama,tanggal,nomor,level,vo2,rating,usia;
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_hasil,null);
		}
		
		final ViewHolder holder = new ViewHolder();
		
		holder.nama = (TextView) convertView.findViewById(R.id.list_nama);
		holder.nama.setText(""+nama[position]);
		
		holder.tanggal = (TextView) convertView.findViewById(R.id.list_tanggal);
		holder.tanggal.setText(""+tanggal[position]);
		
		holder.level = (TextView) convertView.findViewById(R.id.list_level);
		holder.level.setText(""+level[position]+"."+shuttle[position]);
		
		holder.vo2 = (TextView) convertView.findViewById(R.id.list_vo2);
		double z = Double.parseDouble(vo2[position]);
		holder.vo2.setText(new DecimalFormat("##.###").format(z));
		
		holder.rating = (TextView) convertView.findViewById(R.id.list_rating);
		holder.rating.setText(""+rating[position]);
		
		holder.usia = (TextView) convertView.findViewById(R.id.list_usia);
		holder.usia.setText(""+usia[position]);
		
		return convertView;
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	

}
