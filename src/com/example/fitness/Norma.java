package com.example.fitness;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

public class Norma extends Activity implements OnClickListener{

	Button back;
	ImageView image1,image2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_norma);
		
		back = (Button) findViewById(R.id.back);
		back.setOnClickListener(this);
		
		image1 = (ImageView) findViewById(R.id.image1);
		image2 = (ImageView) findViewById(R.id.image2);
		
		Zoomy.Builder builder = new Zoomy.Builder(this)
		.target(image1);
		builder.register();
		
		Zoomy.Builder builder2 = new Zoomy.Builder(this)
		.target(image2);
		builder2.register();
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back:
			onBackPressed();
			break;
		}
	}

}
