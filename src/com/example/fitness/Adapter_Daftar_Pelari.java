package com.example.fitness;

import java.util.ArrayList;
import java.util.List;

import com.example.fitness.R.color;


import android.R.layout;
import android.content.Context;
import android.graphics.Color;
import android.renderscript.Element.DataKind;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Adapter_Daftar_Pelari extends ArrayAdapter<Data_Pelari>{
	Context c;
	LayoutInflater inflater;
	
	
	public Adapter_Daftar_Pelari(Context context, List<Data_Pelari> dataku) {
		super(context, R.layout.model_daftar_pelari,dataku);
		
		this.c = context;
	}
	
	public class ViewHolder
	{
		TextView nama,gender,usia,aktif;
		LinearLayout layout;
		
	}
	
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Data_Pelari data = getItem(position);
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_daftar_pelari,null);
		}
		final ViewHolder holder = new ViewHolder();
		
		holder.nama = (TextView) convertView.findViewById(R.id.teks_nama);
		holder.nama.setText(""+data.nama);
		holder.gender = (TextView) convertView.findViewById(R.id.teks_gender);
		if(data.gender==1)
		{
			holder.gender.setText("Laki-Laki");
		}
		else
		{
			holder.gender.setText("Perempuan");
		}
		holder.usia = (TextView) convertView.findViewById(R.id.teks_usia);
		holder.usia.setText(data.usia+" tahun");
		
		holder.layout = (LinearLayout) convertView.findViewById(R.id.layout_aktif); 
		holder.aktif = (TextView) convertView.findViewById(R.id.teks_aktif);
		if(data.aktif==1)
		{
			holder.aktif.setText("Aktif");
			holder.layout.setBackgroundColor(Color.parseColor("#5CE681"));
		}
		else
		{
			holder.aktif.setText("Non aktif");
			holder.layout.setBackgroundColor(Color.parseColor("#FF7466"));
		}
		
		return convertView;
	}

}
