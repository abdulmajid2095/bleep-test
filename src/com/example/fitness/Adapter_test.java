package com.example.fitness;

import java.util.ArrayList;
import java.util.List;

import com.example.fitness.R.color;


import android.R.layout;
import android.content.Context;
import android.graphics.Color;
import android.renderscript.Element.DataKind;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Adapter_test extends ArrayAdapter<Data_Pelari>{
	Context c;
	LayoutInflater inflater;
	
	
	public Adapter_test(Context context, List<Data_Pelari> dataku) {
		super(context, R.layout.model_test,dataku);
		
		this.c = context;
	}
	
	public class ViewHolder
	{
		TextView nomor,nama;
		
	}
	
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Data_Pelari data = getItem(position);
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_test,null);
		}
		final ViewHolder holder = new ViewHolder();
		
		holder.nama = (TextView) convertView.findViewById(R.id.teks_nama);
		holder.nama.setText(""+data.nama);
		
		holder.nomor = (TextView) convertView.findViewById(R.id.teks_nomor);
		int x = position+1;
		holder.nomor.setText(""+x);
		
		
		
		return convertView;
	}

}
