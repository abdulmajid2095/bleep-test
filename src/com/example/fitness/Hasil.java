package com.example.fitness;

import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class Hasil extends Activity implements OnClickListener,  OnRefreshListener, android.widget.AdapterView.OnItemLongClickListener{

	SwipeRefreshLayout swipeRefreshLayout;
	List<Data_Hasil> dataku;
	ListView list;
	long id_dipilih;
	Button back;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_hasil);
		
		back = (Button) findViewById(R.id.back);
		back.setOnClickListener(this);
		
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);
		list = (ListView) findViewById(R.id.list);
		list.setOnItemLongClickListener(this);
		dataku = Data_Hasil.listAll(Data_Hasil.class);
		Adapter_Hasil adapter = new Adapter_Hasil(Hasil.this, dataku);
		list.setAdapter(adapter);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back:
			onBackPressed();
			break;
		}
	}
	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int posisi,
			long arg3) {
		// TODO Auto-generated method stub
		id_dipilih = dataku.get(posisi).id;
		dialog();
		return true;
	}
	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		swipeRefreshLayout.setRefreshing(true);
		List<Data_Hasil> dataku= Data_Hasil.listAll(Data_Hasil.class);
		Adapter_Hasil adapter = new Adapter_Hasil(Hasil.this, dataku);
		list.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		swipeRefreshLayout.setRefreshing(false);
	}
	
	
	public void dialog()
	{
		final Dialog dialog = new Dialog(this);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    dialog.setContentView(R.layout.model_dialog_pilihan_hapus);
	    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	    Button hapus = (Button) dialog.findViewById(R.id.hapus);
	    hapus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				dialog_hapus();
			}
		});
	    
		dialog.show();
	}
	
	public void dialog_hapus()
	{
		final Dialog dialog = new Dialog(this);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    dialog.setContentView(R.layout.model_dialog_hapus);
	    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	    Button ya = (Button) dialog.findViewById(R.id.ya);
	    ya.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				Data_Hasil.deleteAll(Data_Hasil.class,"id = ?",""+id_dipilih);
				onRefresh();
			}
		});
	    Button tidak = (Button) dialog.findViewById(R.id.tidak);
	    tidak.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				
			}
		});
	    
		dialog.show();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		finish();
		Intent intent = new Intent(Hasil.this,Utama.class);
		startActivity(intent);
	}

	
}
