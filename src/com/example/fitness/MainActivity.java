package com.example.fitness;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
       
        /*Data_Pelari.deleteAll(Data_Pelari.class);
        Data_Hasil.deleteAll(Data_Hasil.class);*/
        
        new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				
				Intent lanjut = new Intent(MainActivity.this, Utama.class);
    			startActivity(lanjut);
			}
		},2000);
    }


    
}
