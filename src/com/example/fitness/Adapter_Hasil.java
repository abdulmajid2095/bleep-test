package com.example.fitness;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes.Name;

import com.example.fitness.R.color;


import android.R.layout;
import android.content.Context;
import android.graphics.Color;
import android.renderscript.Element.DataKind;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Adapter_Hasil extends ArrayAdapter<Data_Hasil>{
	Context c;
	LayoutInflater inflater;
	
	
	public Adapter_Hasil(Context context, List<Data_Hasil> dataku) {
		super(context, R.layout.model_hasil,dataku);
		
		this.c = context;
	}
	
	public class ViewHolder
	{
		TextView nama,tanggal,nomor,level,vo2,rating,usia;
		
	}
	
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Data_Hasil data = getItem(position);
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_hasil,null);
		}
		final ViewHolder holder = new ViewHolder();
		
		holder.nama = (TextView) convertView.findViewById(R.id.list_nama);
		holder.nama.setText(""+data.nama);
		
		holder.tanggal = (TextView) convertView.findViewById(R.id.list_tanggal);
		holder.tanggal.setText(""+data.tanggal);
		
		holder.level = (TextView) convertView.findViewById(R.id.list_level);
		holder.level.setText(""+data.level);
		
		holder.vo2 = (TextView) convertView.findViewById(R.id.list_vo2);
		double z = Double.parseDouble(data.vo2);
		holder.vo2.setText(new DecimalFormat("##.###").format(z));
		
		holder.rating = (TextView) convertView.findViewById(R.id.list_rating);
		holder.rating.setText(""+data.rating);
		
		holder.usia = (TextView) convertView.findViewById(R.id.list_usia);
		holder.usia.setText(""+data.usia);
		
		return convertView;
	}

}
