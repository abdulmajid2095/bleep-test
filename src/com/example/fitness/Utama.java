package com.example.fitness;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;


public class Utama extends Activity implements OnClickListener{

	Button m1,m2,m3,m4,profile;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_utama);
		
		m1 = (Button) findViewById(R.id.menu1);
		m1.setOnClickListener(this);
		m2 = (Button) findViewById(R.id.menu2);
		m2.setOnClickListener(this);
		m3 = (Button) findViewById(R.id.menu3);
		m3.setOnClickListener(this);
		m4 = (Button) findViewById(R.id.menu4);
		m4.setOnClickListener(this);
		profile = (Button) findViewById(R.id.menu_profile);
		profile.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.menu1:
			Intent intent = new Intent(Utama.this,Panduan.class);
			startActivity(intent);
			break;
		
		case R.id.menu2:
			Intent intent2 = new Intent(Utama.this,Test.class);
			startActivity(intent2);
			break;
			
		case R.id.menu3:
			Intent intent3 = new Intent(Utama.this,Norma.class);
			startActivity(intent3);
			break;
	
		case R.id.menu4:
			Intent intent4 = new Intent(Utama.this,Hasil.class);
			startActivity(intent4);
			break;
			
		case R.id.menu_profile:
			Intent intent5 = new Intent(Utama.this,Profil.class);
			startActivity(intent5);
			break;

		}
	}

	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		Intent intent = new Intent(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_HOME);
	    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    startActivity(intent);
	}
	

}
